<?php

namespace Drupal\vexflow\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextareaWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'vexflow_textarea' widget.
 *
 * @FieldWidget(
 *   id = "vexflow_textarea",
 *   module = "vexflow",
 *   label = @Translation("Vexflow"),
 *   field_types = {
 *     "vexflow"
 *   }
 * )
 */
class VexflowWidget extends StringTextareaWidget {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'textarea',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
    ];

    return $element;
  }

}
