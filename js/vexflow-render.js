/**
 * @file
 * Javascript for the location autocomplete.
 */

(function ($, Drupal) {
  /**
   * Google map location autocomplete.
   *
   * @type {Drupal~behavior}
   * @type {Object} drupalSettings.vexflow
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches functionality to relevant elements.
   */
  Drupal.behaviors.vexfllow = {
    attach(context, drupalSettings) {
      $('#vexflow-notes', context).once('backtotop').each(function () {
        const notation = $(this).text()
        const VF = Vex.Flow;

        const vf = new VF.Factory({
          renderer: {elementId: 'vexflow-notes', width: 500, height: 200}
        });

        const score = vf.EasyScore();
        const system = vf.System();

        system.addStave({
          voices: [
            score.voice(score.notes(notation)),
          ]
        }).addClef('treble').addTimeSignature('4/4');

        vf.draw();

      });
    }
  };
})(jQuery, Drupal);
