# Introduction
Vexflow module integrates the [0xfe/vexflow](https://github.com/0xfe/vexflow) open-source library for online music notation
rendering.

## Installing with composer:
* Add following line to your composer.json file
* Copy flollowing snippet into your project's composer.json file
```
"repositories": {
    "0xfe/vexflow": {
       "type": "package",
       "package": {
           "name": "0xfe/vexflow",
           "version": "3.0.9",
           "type": "drupal-library",
           "dist": {
               "url": "https://github.com/0xfe/vexflow/archive/3.0.9.zip",
               "type": "zip"
           }
       }
    }
}
```
* Run `$ composer require 'drupal/vexflow:1.0.x-dev'`
* The library is downloaded in `/libraries/vexflow` directory.
* Create the field with "Vexflow notations" field type.
* Create content and add [EasyScore](https://github.com/0xfe/vexflow/wiki/Using-EasyScore) notations in the field.
* The EasyScore notations will be rendered as notations.
